# A Joke Generator APP.

A small app called Chuck Norris Joke Generator, created with vanilaJS and AJAX to fetch from Chuck Norris Joke Generator API.

### Prerequisites

None

### Installing

git clone https://github.com/letowebdev/Joke-Generator-using-API.git


## Deployment

http://zacheleto.me/Projects/JokeGenerator/

## Built With

* [Plain JavaScript]
* [Chuck Norris Joke Generator API]
* [Skeleton CDN]


## Author

* **Zache Abdelatif (Leto)** 

## License

This project is licensed under the MIT License
